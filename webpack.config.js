var path = require("path");
var ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
    entry: "./src/AppInit.js",
    output: {
        path: path.join(__dirname, "web"),
        filename: "app.js"
    },
    module: {
        loaders: [
            { test: /\.css$/, loader: ExtractTextPlugin.extract(
                "style-loader", "css-loader!autoprefixer-loader") },
            { test: /\.jsx$/, loader: "jsx-loader" },
            { test: /\.(png|jgp|jpeg|gif|svg)$/, loader: "url-loader?limit=10000" },
        ]
    },
    resolve: {
        modulesDirectories: ["src", "node_modules", "bower_modules"],
        extensions: ["", ".js", ".jsx"],
    },
    plugins: [
        new ExtractTextPlugin("app.css"),
    ],
};

