
# Sample React Application

Author: [Erki Esken](mailto:erki@esken.net)

This is a sample repo with beginnings of a [React](https://facebook.github.io/react/) application built with [Webpack](http://webpack.github.io).


# Getting started

Just run `npm install` and then `npm run-script dev-server` to see the local development site.

# Licence

The code in this repository is licensed under [The MIT license](http://opensource.org/licenses/MIT).

