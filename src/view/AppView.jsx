
require("../style/main.css");

var React = require("react");
var Reflux = require("reflux");

var CalendarActions = require("../action/CalendarActions");

var CalendarStore = require("../store/CalendarStore");

var HeaderView = React.createClass({

    propTypes: {
        text: React.PropTypes.string.isRequired,
    },

    getDefaultProps: function ()
    {
        return {
            text: "Default header"
        };
    },

    render: function ()
    {
        return <header>
            <h1>{this.props.text}</h1>
            {this.props.children}
        </header>;
    },

});



var AppView = React.createClass({

    mixins: [
        Reflux.listenTo(CalendarStore, "forceUpdate"),
    ],

    componentDidMount: function ()
    {
        setInterval(this.forceUpdate.bind(this), 1000);
    },

    render: function ()
    {
        var now = CalendarStore.getLastClick();
        var time = "never clicked";

        if (now) {
            time = now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
        }

        return <div className="app-container" onClick={this._onClick}>
            <HeaderView>
                <p>Blah</p>
            </HeaderView>
            <HeaderView text="Fooasdasda"/>
            <p>Hello.</p>
            <p>Current time is: {time}</p>
        </div>;
    },

    _onClick: function (ev)
    {
        CalendarActions.setDate(new Date());

        console.log(ev);
    },

});

module.exports = AppView;

