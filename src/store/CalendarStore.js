
var Reflux = require("reflux");

var CalendarActions = require("../action/CalendarActions");

var _data = {
    lastClick: null,
};

var Store = Reflux.createStore({

    listenables: [
        CalendarActions,
    ],

    onSetDate: function (date)
    {
        console.log("set date to", date);

        _data.lastClick = date;

        this.trigger();
    },

    /**
     *
     */
    getLastClick: function ()
    {
        return _data.lastClick;
    },

});


module.exports = Store;


