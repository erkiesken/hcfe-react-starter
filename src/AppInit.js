
var React = require("react");
var AppView = require("./view/AppView.jsx");

window.React = React;

React.render(
    React.createElement(AppView),
    document.body
);

